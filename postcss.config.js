module.exports = {
	plugins: {
		autoprefixer: {
			browsers: [
				"last 1 version",
				"> 1%",
				"not dead"
			]
		}
	},
}