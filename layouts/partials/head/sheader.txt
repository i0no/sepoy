
    {{- with site.Params.sHeader }}
        {{- with .strictTS }}
                {{- printf "%s %s\n" "Strict-Transport-Security:" . -}}
            {{- end }}
        {{- with .frameOptions }}
                {{- printf "%s %s\n" "X-Frame-Options:" . -}}
            {{- end }}
        {{- with .contentTypeOptions }}
                {{- printf "%s %s\n" "X-Content-Type-Options:" . -}}
            {{- end }}
        {{- with .xss }}
                {{- printf "%s %s\n" "X-XSS-Protection:" . -}}
            {{- end }}
        {{- with .refPolicy }}
                {{- printf "%s %s\n" "Referrer-Policy:" . -}}
            {{- end }}
        {{- with .permPolicy }}
                {{- printf "%s %s\n" "Permissions-Policy:" . -}}
            {{- end }}
        {{- with .accControl }}
                {{- printf "%s %s\n" "Access-Control-Allow-Origin:" . -}}
            {{- end }}
    {{- end }}
